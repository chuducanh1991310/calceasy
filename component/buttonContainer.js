import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import ButtonCalc from './button';

export default class ButtonContainer extends Component {
    render () {
        return(
        <View style = {styles.Container}>
            <View style  = {{flex: 1, flexDirection: 'row',}}>
                <ButtonCalc operator = '+' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '-' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '*' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '/' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
            </View>
            
            <View style  = {{flex: 1, flexDirection: 'row',}}>
                <ButtonCalc operator = '1' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '2' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '3' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
            </View>

            <View style  = {{flex: 1, flexDirection: 'row',}}>
                <ButtonCalc operator = '4' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '5' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '6' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
            </View>
            <View style = {{flex: 1, flexDirection: 'row',}}>
                <ButtonCalc operator = '7' Funtion = {(operator) => {this.props.handleButtonPress(operator)}} />
                <ButtonCalc operator = '8' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '9' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
            </View>
            <View style = {{flex: 1, flexDirection: 'row',}}>
                <ButtonCalc operator = '0' Funtion = {(operator) => {this.props.handleButtonPress(operator)}} />
                <ButtonCalc operator = '.' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
                <ButtonCalc operator = '=' Funtion = {(operator) => {this.props.handleButtonPress(operator)}}/>
            </View>
        </View>
        ) 
    }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
    },
})